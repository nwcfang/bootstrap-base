var gulp = require('gulp');
var autoprefixer = require('gulp-autoprefixer');
var sass = require('gulp-sass');
var pug = require('gulp-pug');
var concat = require('gulp-concat');
var plumber = require('gulp-plumber');
var sourcemaps = require('gulp-sourcemaps');

gulp.task('sass', function() {
  return gulp.src('src/scss/*.scss')
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(sass({precision: 8}))
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('dist/css'));
});

gulp.task('scripts', function() {
  return gulp.src('node_modules/bootstrap-sass/assets/javascripts/**/*.js')
    .pipe(concat('all.js'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('views', function() {
  return gulp.src('src/*.pug')
    .pipe(plumber())
    .pipe(pug({
      pretty: true
    }))
    .pipe(gulp.dest('dist'));
});

gulp.task('assets', function() {
  return gulp.src(['src/assets/*', 'node_modules/bootstrap-sass/assets/**'])
    .pipe(gulp.dest('dist/assets'));
});


gulp.task('watch', function() {
  gulp.watch('src/assets/*', ['assets']);
  gulp.watch('src/scss/*.scss', ['sass']);
  gulp.watch('src/**/*.pug', ['views'])
});

gulp.task('default', [ 'sass', 'views', 'scripts', 'assets', 'watch']);
